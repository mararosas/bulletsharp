﻿/*This file is part of BulletSharp
    2014 Max J. Rodríguez Beltran ing.maxjrb[at]gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Windows.Forms;
using BulletSharp.Properties;

namespace BulletSharp
{
    public partial class frmSettings : Form
    {
        public frmSettings()
        {
            InitializeComponent();
        }

        private void linkLabel1_Click(object sender, EventArgs e)
        {
          SaveSettings();
        }

        private void txtAPIKEY_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) Keys.Enter)
            {
                SaveSettings();
            }
        }

        private void SaveSettings()
        {
            if (txtAPIKEY.TextLength <= 0)
            {
                MessageBox.Show("API Key must have a value...", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                Settings.Default.AcceptCerts = chkSSL.Checked;
                Settings.Default.ApiKey = txtAPIKEY.Text.Trim();
                Settings.Default.Save();
                Close();
            }
        }

        private void txtAPIKEY_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
